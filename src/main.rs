use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

mod debug;

use debug::DebugPlugin;
fn main() {
    App::new()
        .insert_resource(ClearColor(Color::hsl(0.0, 0.5, 0.3)))
        .insert_resource(WindowDescriptor {
            title: "Bevy Playground".to_string(),
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(RapierDebugRenderPlugin::default())
        .add_plugin(DebugPlugin)
        .add_startup_system(setup_graphics)
        .add_startup_system(setup_physics)
        .add_system(inputs)
        .run();
}

fn setup_graphics(mut commands: Commands) {
    commands.spawn_bundle(PerspectiveCameraBundle {
        transform: Transform::from_xyz(-3.0, 3.0, 10.0).looking_at(Vec3::ZERO, Vec3::Y),
        ..Default::default()
    });
}

fn setup_physics(mut commands: Commands) {
    commands
        .spawn()
        .insert(Name::new("Ground"))
        .insert(Collider::cuboid(100.0, 0.1, 100.0))
        .insert_bundle(TransformBundle::from(Transform {
            translation: Vec3::new(0.0, -2.0, 0.0),
            rotation: Quat::from_axis_angle(Vec3::X, -0.05),
            ..Default::default()
        }));

    commands
        .spawn()
        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(0.5))
        .insert(Name::new("Ball"))
        .insert(ExternalForce::default())
        .insert(Damping {
            angular_damping: 10.0,
            linear_damping: 0.0,
        })
        .insert(Restitution::coefficient(0.9))
        .insert_bundle(TransformBundle::from(Transform::from_xyz(0.0, 20.0, 5.0)));

    commands
        .spawn()
        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(0.2))
        .insert(Name::new("Small ball"))
        .insert(Restitution::coefficient(1.0))
        .insert_bundle(TransformBundle::from(Transform::from_xyz(0.0, 10.0, 0.0)));
}

fn inputs(mut forced: Query<&mut ExternalForce>, keyboard: Res<Input<KeyCode>>) {
    for mut entity in forced.iter_mut() {
        let mut vector = Vec3::ZERO;
        if keyboard.pressed(KeyCode::Up) {
            vector -= Vec3::new(0.0, 0.0, 1.0);
        }
        if keyboard.pressed(KeyCode::Down) {
            vector += Vec3::new(0.0, 0.0, 1.0);
        }
        if keyboard.pressed(KeyCode::Left) {
            vector -= Vec3::new(1.0, 0.0, 0.0);
        }
        if keyboard.pressed(KeyCode::Right) {
            vector += Vec3::new(1.0, 0.0, 0.0);
        }

        entity.force = vector * 10.0;
    }
}
