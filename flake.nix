# Adapted from https://srid.ca/rust-nix
{
  description = "Getting started with the Bevy game engine for Rust";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
    rust-overlay.url = "github:oxalica/rust-overlay";
    crate2nix = {
      url = "github:kolloch/crate2nix";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat, rust-overlay, crate2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "rust-bevy-playground";
        pkgs = import nixpkgs {
          inherit system;
          verlays = [
            rust-overlay.overlay
            (self: super: {
                # Because rust-overlay bundles multiple rust packages into one
                # derivation, specify that mega-bundle here, so that crate2nix
                # will use them automatically.
                rustc = self.rust-bin.stable.latest.default;
                cargo = self.rust-bin.stable.latest.default;
              })
          ];
        };

        inherit (import "${crate2nix}/tools.nix" { inherit pkgs; }) generatedCargoNix;

        # Create cargo2nix project
        project = pkgs.callPackage
          (generatedCargoNix {
            inherit name;
            src = ./.;
          })
          {
            # Individual crate overrides go here
            # Example: https://github.com/balsoft/simple-osd-daemons/blob/6f85144934c0c1382c7a4d3a2bbb80106776e270/flake.nix#L28-L50
            defaultCrateOverrides = pkgs.defaultCrateOverrides // {
              # The app crate itself is overridden here. Typically we
              # configure non-Rust dependencies (see below) here.
              ${name} = oldAttrs: {
                inherit buildInputs nativeBuildInputs;
              } // buildEnvVars;
            };
          };

        # Configuration for the non-Rust dependencies
        buildInputs = with pkgs; [
          openssl.dev
        ];
        nativeBuildInputs = with pkgs; [
          rustc
          cargo
          pkgconfig
          lld
          vulkan-loader
          alsa-lib
          udev
          wayland
          libxkbcommon
          nixpkgs-fmt
          rust-analyzer
          rustfmt
          gnumake
          jq
          cachix
        ];
        buildEnvVars = {
          PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
        };


      in rec {
        packages.${name} = project.rootCrate.build;
        defaultPackage = packages.${name};

        apps.${name} = flake-utils.lib.mkApp {
          inherit name;
          drv = packages.${name};
        };
        dafaultApp = apps.${name};

        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
          AMD_VULKAN_ICD = "RADV";
          XCURSOR_THEME = "Adwaita"; # Workaround for https://github.com/bevyengine/bevy/issues/4768
          LD_LIBRARY_PATH = "$LD_LIBRARY_PATH:${with pkgs; lib.makeLibraryPath [
            udev alsa-lib vulkan-loader
            libxkbcommon wayland # To use wayland feature
          ]}";
        };
      }
    );
}
